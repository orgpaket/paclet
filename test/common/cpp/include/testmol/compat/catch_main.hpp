// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

#ifndef TESTMOL_COMPAT_CATCH_MAIN_HPP
#define TESTMOL_COMPAT_CATCH_MAIN_HPP

#include <testmol/compat/data.hpp>
#include <testmol/compat/detail/runner.hpp>

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define TESTMOL_CATCH_MAIN(suffix)                                                                 \
                                                                                                   \
  /* NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)*/                          \
  extern testmol::compat::data::path project_prefix;                                               \
  /* NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)*/                          \
  extern testmol::compat::data::path group_prefix;                                                 \
                                                                                                   \
  /* path_prefix : Paths Corresponding to the Complete Project */                                  \
  /* NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)*/                          \
  testmol::compat::data::path project_prefix = testmol::compat::data::path();                      \
  /* path_prefix : Paths Corresponding to The Current Group */                                     \
  /* NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)*/                          \
  testmol::compat::data::path group_prefix = testmol::compat::data::path();                        \
                                                                                                   \
  auto main(int argc, char* argv[])->int                                                           \
  {                                                                                                \
    return testmol::compat::detail::runner(                                                        \
      argc, argv, &project_prefix, &group_prefix, std::string(suffix));                            \
  }

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define TESTMOL_CATCH_GROUP()                                                                      \
                                                                                                   \
  /* NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)*/                          \
  extern testmol::compat::data::path project_prefix;                                               \
  /* NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)*/                          \
  extern testmol::compat::data::path group_prefix;

#endif // TESTMOL_COMPAT_CATCH_MAIN_HPP
