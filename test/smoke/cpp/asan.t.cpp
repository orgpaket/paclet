// -----------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: (C) 2022-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -----------------------------------------------------------------------------

#include <cstdlib>

auto main(int argc, char** /*argv*/) -> int
{
  int* array = new int[100]; // NOLINT
  delete[] array;            // NOLINT
  return array[argc];        // NOLINT
}
