# ------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
# ------------------------------------------------------------------------------

add_executable(smoke.disabled.t disabled.t.cpp)
add_test(smoke.disabled.t smoke.disabled.t)
set_property(TEST smoke.disabled.t PROPERTY LABELS smoke_test)
target_link_libraries(smoke.disabled.t PRIVATE libtestpaket)

add_executable(smoke.thread.t thread.t.cpp)
add_test(smoke.thread.t smoke.thread.t)
set_property(TEST smoke.thread.t PROPERTY LABELS smoke_test)
target_link_libraries(smoke.thread.t PRIVATE libtestpaket)

if(DIAGNOSTIC_ASAN)
  add_executable(smoke.asan.t asan.t.cpp)
  add_test(smoke.asan.t smoke.asan.t)
  target_link_libraries(smoke.asan.t PRIVATE tora::instrument.asan.cxx)
  set_tests_properties(smoke.asan.t PROPERTIES WILL_FAIL TRUE)
  set_property(TEST smoke.asan.t PROPERTY LABELS smoke_test)
endif()


if(DIAGNOSTIC_LSAN)
  add_executable(smoke.lsan.t lsan.t.cpp)
  add_test(smoke.lsan.t smoke.lsan.t)
  target_link_libraries(smoke.lsan.t PRIVATE libtestpaket)
  set_tests_properties(smoke.lsan.t PROPERTIES WILL_FAIL TRUE)
  set_property(TEST smoke.lsan.t PROPERTY LABELS smoke_test)
endif()


if(DIAGNOSTIC_UBSAN.N)
  add_executable(smoke.ubsan.n.t ubsan.n.t.cpp)
  add_test(smoke.ubsan.n.t smoke.ubsan.n.t)
  target_link_libraries(smoke.ubsan.n.t PRIVATE libtestpaket)
  set_tests_properties(smoke.ubsan.n.t PROPERTIES WILL_FAIL TRUE)
  set_property(TEST smoke.ubsan.n.t PROPERTY LABELS smoke_test)
endif()



